from django.shortcuts import render, redirect, get_object_or_404
from django.contrib.auth.decorators import login_required
from projects.forms import create_project_form
from projects.models import Project

# Create your views here.


@login_required
def list_projects(request):
    list_projects = Project.objects.filter(owner=request.user)
    context = {
        "list_projects": list_projects,
    }
    return render(request, "projects/list_projects.html", context)


@login_required
def show_project(request, id):
    project_detail = get_object_or_404(Project, id=id)
    context = {
        "project_detail": project_detail,
    }
    return render(request, "projects/show_project.html", context)


@login_required
def create_project(request):
    if request.method == "POST":
        form = create_project_form(request.POST)
        if form.is_valid():
            Project = form.save(False)
            Project.owner = request.user
            Project.save()
            return redirect("/projects/")
    else:
        form = create_project_form()

    context = {
        "form": form,
    }
    return render(request, "projects/create.html", context)
