from django.shortcuts import render, redirect
from django.contrib.auth.decorators import login_required
from tasks.forms import create_task_form
from tasks.models import Task

# Create your views here.


@login_required
def create_task(request):
    if request.method == "POST":
        form = create_task_form(request.POST)
        if form.is_valid():
            form.save()
            return redirect("/projects/")
    else:
        form = create_task_form()
    context = {
        "form": form,
    }
    return render(request, "tasks/create.html", context)


@login_required
def show_my_tasks(request):
    task_list = Task.objects.filter(assignee=request.user)
    context = {
        "task_list": task_list,
    }
    return render(request, "tasks/show_my_tasks.html", context)
